@php
    use App\Models\KodeTms;

@endphp

<html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title></title>
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>

<body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-success">
        <div class="container">
          <a class="navbar-brand" href="#">Kpu Kota Pasuruan</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                  </li>
          </div>
        </div>
      </nav>
    <h1>PENGAJUAN DATA BARU</h1>
    <div class="container">
        @if(count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            {{ $error }} <br/>
            @endforeach
        </div>
        @endif
        <form action="{{ route('postDataBaru') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>NKK</th>
                                <td>
                                    <input type="text" name="nkk" value="" style="text-transform:uppercase">
                                </td>
                            </tr>
                            <tr>
                                <th>NIK</th>
                                <td>
                                    <input type="text" name="nik" value="" style="text-transform:uppercase">
                                </td>
                            </tr>
                            <tr>
                                <th>NAMA</th>
                                <td>
                                    <input type="text" name="nama" value="" style="text-transform:uppercase">
                                </td>
                            </tr>
                            <tr>
                                <th>TEMPAT LAHIR</th>
                                <td>
                                    <input type="text" name="tempat_l" value="" style="text-transform:uppercase">
                                </td>
                            </tr>
                            <tr>
                                <th>TANGGAL LAHIR</th>
                                <td>
                                    <input type="date" name="tanggal_l" value="">
                                </td>
                            </tr>
                            <tr>
                                <th>STATUS PERNIKAHAN</th>
                                <td>
                                    <input type="text" name="status" value="" style="text-transform:uppercase">
                                </td>
                            </tr>
                            <tr>
                                <th>JENIS KELAMIN</th>
                                <td>
                                    <input type="text" name="jenkel" value="" style="text-transform:uppercase">
                                </td>
                            </tr>
                            <tr>
                                <th>JALAN / DUKUH</th>
                                <td>
                                    <input type="text" name="jln_dukuh" value="" style="text-transform:uppercase">
                                </td>
                            </tr>
                            <tr>
                                <th>RT</th>
                                <td>
                                    <input type="number" name="rt" value="">
                                </td>
                            </tr>
                            <tr>
                                <th>RW</th>
                                <td>
                                    <input type="number" name="rw" value="">
                                </td>
                            </tr>
                            <tr>
                                <th>DISABILITAS</th>
                                <td>
                                    <div class="">
                                        <select name="disabilitas" id="disabilitas">

                                            <option value="1">Tidak Cacat</option>
                                            <option value="2">Tuna Daksa/Cacat Tubuh</option>
                                            <option value="3">Tuna Netra/Buta</option>
                                            <option value="4">Cacat Mental Retardasi</option>
                                            <option value="5"> Tuna Wicara</option>
                                            <option value="6">Mantan Penderita Gangguan Jiwa</option>
                                            <option value="7">Tuna Rungu</option>
                                            <option value="8">Cacat Fisik dan Mental</option>
                                            <option value="9">Tuna Rungu & Wicara</option>
                                            <option value="10">Tuna Rungu & Wicara & Cacat Tubuh</option>
                                            <option value="11">Tuna Rungu & Wicara & Netra & Cacat
                                                Tubuh</option>
                                            <option value="12">Tuna Netra & Cacat Tubuh</option>
                                            <option value="13">Tuna Netra & Rungu & Wicara</option>




                                            {{-- <option value="1">Meninggal dunia</option>
                                            <option value="2">Ditemukan data ganda</option>
                                            <option value="3">Dibawah Umur</option>
                                            <option value="4">Pindah Domisili</option>
                                            <option value="5">Tidak Dikenal</option>
                                            <option value="6">TNI</option>
                                            <option value="7">Polri</option>
                                            <option value="8">Hilang ingatan</option>
                                            <option value="9">Hak Pilih di Cabut</option>
                                            <option value="10">Bukan Penduduk</option> --}}
                                        </select>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="img-container"></div>
                    <h4>Sertakan Dokumen Bukti berupa foto KTP dan KK</h4>
                    <input type="file" id="pendukung" name="pendukung[]" accept="image/*" onchange="updatepreview()"
                        multiple>
                </div>
            </div>

            <div class="row mb-2">
                <input type="submit">
            </div>
        </form>


    </div>

    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        function updatepreview() {
            const inpEl = document.getElementById('pendukung')
            document.getElementById('img-container').innerHTML = ''
            for (const el of inpEl.files) {
                let reader = new FileReader()
                reader.onload = function(e) {
                    const newImg = document.createElement('img')
                    newImg.src = e.target.result
                    newImg.width = 200
                    document.getElementById('img-container').appendChild(newImg)
                }
                reader.readAsDataURL(el)
            }
        }
    </script>

</body>
