<html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title></title>
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .warna{
            width: 15px;
            height: 5px;
            border: solid 3px black ;
            background: red;
        }
    </style>
</head>

<body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    @php
        use App\Models\KodeTms;
    @endphp
     <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-success">
        <div class="container">
          <a class="navbar-brand" href="#">Kpu Kota Pasuruan</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                  </li>
          </div>
        </div>
      </nav>
      <br><br><br>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <tbody>
                        <tr>
                            <th>NKK</th>
                            <td>
                                {{ $data->nkk }}
                            </td>
                        </tr>
                        <tr>
                            <th>NIK</th>
                            <td>
                                {{ $data->nik }}
                            </td>
                        </tr>
                        <tr>
                            <th>NAMA</th>
                            <td>
                                {{ $data->nama }}
                            </td>
                        </tr>
                        <tr>
                            <th>TEMPAT LAHIR</th>
                            <td>
                                {{ $data->tempat_l }}
                            </td>
                        </tr>
                        <tr>
                            <th>TANGGAL LAHIR</th>
                            <td>
                                {{ $data->tanggal_l }}
                            </td>
                        </tr>
                        <tr>
                            <th>STATUS PERNIKAHAN</th>
                            <td>
                                {{ $data->status }}
                            </td>
                        </tr>
                        <tr>
                            <th>JENIS KELAMIN</th>
                            <td>
                                {{ $data->jenkel }}
                            </td>
                        </tr>
                        <tr>
                            <th>JALAN / DUKUH</th>
                            <td>
                                {{ $data->jln_dukuh }}
                            </td>
                        </tr>
                        <tr>
                            <th>RT</th>
                            <td>
                                {{ $data->rt }}
                            </td>
                        </tr>
                        <tr>
                            <th>RW</th>
                            <td>
                                {{ $data->rw }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
        </div>
        @if ($data->data_tms_id !== null)
            <h5>Pemilih Tidak Memenuhi Syarat Karena</h5>
        <h5>{{ KodeTms::find($data->tms->kode)->deskripsi }}</h5>
        @else
            <div class="">
                <a href="{{ route('ubahData', [$data, $nik, $nama]) }}" class="btn btn-primary">Pengajuan Ubah
                    Data</a>
            </div>
            <br>
            <div class="">
                <a href="{{ route('tmsData', [$data, $nik, $nama]) }}" class="btn btn-primary">Pengajuan Data Tidak
                    Memenuhi Syarat</a>
            </div>
        @endif


    </div>

    <script src="{{ asset('js/app.js') }}"></script>

</body>
