<html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title></title>
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-success">
        <div class="container">
          <a class="navbar-brand" href="#">Kpu Kota Pasuruan</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                  </li>
          </div>
        </div>
      </nav>
      <br><br><br>
    @php
        use App\Models\KodeTms;
        similar_text(strtoupper($nama), $data->nama, $percent);
        $allowed = $percent > 70 && $nik === $data->nik;
    @endphp

    @if ($allowed)

        <h1>PENGAJUAN DATA TIDAK MEMENUHI SYARAT</h1>

        <div class="container">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }} <br />
                    @endforeach
                </div>
            @endif
            <form action="{{ route('postDataTms') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="data_pemilih_id" value="{{ $data->id }}">
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>NKK</th>
                                    <td>
                                        {{ $data->nkk }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>NIK</th>
                                    <td>
                                        {{ $data->nik }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>NAMA</th>
                                    <td>
                                        {{ $data->nama }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>TEMPAT LAHIR</th>
                                    <td>
                                        {{ $data->tempat_l }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>TANGGAL LAHIR</th>
                                    <td>
                                        {{ $data->tanggal_l }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>STATUS PERNIKAHAN</th>
                                    <td>
                                        {{ $data->status }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>JENIS KELAMIN</th>
                                    <td>
                                        {{ $data->jenkel }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>JALAN / DUKUH</th>
                                    <td>
                                        {{ $data->jln_dukuh }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>RT</th>
                                    <td>
                                        {{ $data->rt }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>RW</th>
                                    <td>
                                        {{ $data->rw }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="">
                    Alasan tidak memenuhi syarat:
                    <select name="dataTms" id="dataTms">

                        @foreach (KodeTms::all() as $kTms)
                            <option value="{{ $kTms->kode }}">{{ $kTms->deskripsi }}</option>
                        @endforeach

                    </select>
                </div>

                <div id="img-container"></div>
                <input type="file" id="pendukung" name="pendukung[]" accept="image/*" onchange="updatepreview()"
                    multiple>
                <div class="">
                    <input type="submit">
                </div>
            </form>
        </div>

    @else
        <h1>Anda tidak diperkenankan membuka halaman ini.</h1>
    @endif

    <script>
        function updatepreview() {
            const inpEl = document.getElementById('pendukung')
            document.getElementById('img-container').innerHTML = ''
            for (const el of inpEl.files) {
                let reader = new FileReader()
                reader.onload = function(e) {
                    const newImg = document.createElement('img')
                    newImg.src = e.target.result
                    newImg.width = 200
                    document.getElementById('img-container').appendChild(newImg)
                }
                reader.readAsDataURL(el)
            }
        }
    </script>

    <script src="{{ asset('js/app.js') }}"></script>

</body>
