<html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="">
    <link href="/css/app.css" rel="stylesheet">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .more-text {
            max-height: 5rem;
            overflow-y: hidden;
        }

        .more-text-collapsed::before {
            content: 'more...';
            background: linear-gradient(0deg, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 0) 100%);
            color: #22f;
            text-align: right;
            position: absolute;
            bottom: 0;
            right: 0;
            width: 100%;
        }
    </style>

</head>


<body>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <nav class="navbar navbar-expand-lg navbar-light bg-success">
        <div class="container">
            <a class="navbar-brand" href="#">Kpu Kota Pasuruan</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#akordion">Apa Itu DPB?</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#kard">Alur</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#isidata">Cek Data</a>
                    </li>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" tabindex="-1" aria-disabled="true">Kontak</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/login" tabindex="-3" aria-disabled="true">Login</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row" style="height: 600px">
            <div class="col-12 d-flex justify-content-center align-items-center">
                <img src="{{ asset('img/low-res-logo.png') }}" style="height: 50px ">
            </div>
        </div>
        <div class="row" id="akordion" style="padding-top:50px;">
            <div class="col-12">
                <div class="accordion" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <strong> Pemutakkhiran DPB Itu? </strong>
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <strong>Pemutakhiran DPB adalah</strong> egiatan untuk memperbarui elemen data dalam DPT
                                Pemilu atau Pemilihan terakhir secara berkelanjutan sesuai amanat Undang-Undang Nomor 7
                                Tahun 2017 tentang Pemilu.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <strong> apa itu DPB? </strong>
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <strong>DPB</strong> adalah sebuah daftar pemilih berkelanjutan yang berisi data pemilih
                                termutakhir hasil kegiatan PDPB berbasis TPS.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <strong> Harus Ada PDPB Sementara Pemilu Masih Lama? </strong>
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Untuk mempermudah proses pemutakhiran data, sebab selama ini DPT seringkali mengalami
                                hambatan serta sebagai tindak lanjut sistem pendaftaran pemilih secara berkelanjutan
                                (continous voter list).
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <strong> Yang Harus Dilakukan Oleh Sobat Pemilih? </strong>
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Pemilih dapat meneliti data diri atau data keluarga dalam database yang telah ada,
                                akurat, dan mutakhir sesuai KTP elektronik.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="kard" style="padding-top: 50px;">
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset('img/1.jpg') }}" class="card-img-top" alt="...">
                    <div class="card-body more-text">
                        <p class="card-text">
                            Pertama, Sobat Pemilih meneliti data diri atau keluarga dalam menu cek data dengan cara
                            mengisikan NIK.</p>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset('img/2.jpg') }}" class="card-img-top" alt="...">
                    <div class="card-body more-text">
                        <p class="card-text">
                            Jika data ada dan telah benar serta akurat, klik sesuai.</p>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset('img/3.jpg') }}" class="card-img-top" alt="...">
                    <div class="card-body more-text">
                        <p class="card-text">
                            Jika data tidak ditemukan/tidak ada, masuklah pada menu Lapor >> Pemilih Baru.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"  style="padding-bottom: 50px;">
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset('img/4.jpg') }}" class="card-img-top" alt="...">
                    <div class="card-body more-text">
                        <p class="card-text">
                            Jika data ditemukan namun terdapat kesalahan atau ketidak-sesuaian, masuklah pada
                            menu<strong> Lapor >> Ubah Data</strong>, isi kolom pertanyaan dan perubahan elemen data
                            yang berubah dan upload dokumen pendukung, siapkan data KTP el/KK/lainnya.</p>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset('img/5.jpg') }}" class="card-img-top" alt="...">
                    <div class="card-body more-text">
                        <p class="card-text">Untuk melaporkan data keluarga yang sudah Tidak Memenuhi Syarat (TMS)
                            sebagai pemilih namun masih ada di cek data, silakan pilih menu <strong> Lapor >> TMS
                            </strong>. Isikan Nama dan NIK yang bersangkutan dan upload dokumen akta kematian, surat
                            pindah luar kota, SK Pengangkatan bagi TNI/Polri atau dokumen pendukung lainnya.</p>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset('img/6.jpg') }}" class="card-img-top" alt="...">
                    <div class="card-body more-text">
                        <p class="card-text">Untuk dapat berpartisipasi dalam Daftar dan Lapor DPB, Sobat Pemilih
                            perlu menyiapkan dokumen guna keperluan verifikasi dan validasi data yang dilaporkan.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col" style="padding-left: 150px">
            <div class="card" style="width: 900px;">
                <h3 class="card-tittle">Cek Data Pemilih</h3>
                <p class="card-text">Untuk mengetahui NIK anda sudah terdaftar sebagai pemilih atau belum, silakan
                    melakukan pengecekan NIK di
                    bawah ini. Cek data ini berlaku untuk pemilih Aktif/terdaftar di wilayah Kota Pasuruhan.</p>
                <p>Masukkan NIK dan nama Anda</p>
                <form action="cekdata" id="isidata" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="input-nik">Masukkan NIK</label>
                        <input type="text" class="form-control" id="input-nik" name="input-nik"
                            aria-describedby="nik-help" placeholder="Masukkan NIK">
                        <small id="nik-help" class="form-text text-muted">NIK harus lengkap.</small>
                    </div>
                    <div class="form-group">
                        <label for="input-nama">Nama Pemilih</label>
                        <input type="text" class="form-control" id="input-nama" name="input-nama"
                            aria-describedby="namaHelp" placeholder="Enter email">
                        <small id="namaHelp" class="form-text text-muted">Nama sesuai KTP.</small>
                    </div>
                    <input type="submit" class="btn btn-primary m-2" value="cek data!">

                    <a class="btn btn-primary m-2" href="/databaru">Ajukan Data Pemilih Baru</a>
                </form>
                <script src="{{ asset('js/app.js') }}"></script>
            </div>
        </div>
    </div>
    </div>
    <script>

        window.addEventListener('load', () => {
            for (const el of document.querySelectorAll('.more-text')) {
                if (el.scrollHeight > el.clientHeight) {
                    el.classList.add('more-text-more')
                    el.classList.add('more-text-collapsed')
                }
            }

            for (const el of document.querySelectorAll('.more-text-more')) {
                el.addEventListener('click', () => {
                    el.classList.toggle('more-text-collapsed')
                    if (!el.classList.contains('more-text-collapsed')) {
                        el.style.maxHeight = 'max-content'
                    } else {
                        el.style.maxHeight = ''
                    }
                })
            }
        })
    </script>
